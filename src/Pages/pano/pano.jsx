import React from "react";
import { Container,Row } from "react-bootstrap";
import './pano.css';

const Pano = () =>{
    return(
        <Container>
            <Row className="blpano">
                <Row className="textpano"> Création de Site vitrine <br/> sur les images 360 </Row>
            <iframe
                title="Pano2VR Viewer"
                src="/pano/index.html" // Update with the actual path to your Pano2VR output HTML file
                width="100%"
                height="600px" // Set the desired height
                frameBorder="0"
                allowFullScreen
            />
        </Row>
      </Container>
    )
}

export default Pano;