import React from "react";
import { Row, Col } from "react-bootstrap";
import { BsFacebook, BsLinkedin,BsMailbox,BsFillPhoneVibrateFill} from 'react-icons/bs';
import './footer.css'

const Footer = () => {

    return(
        <div className="bck">
            <Row className="">
                <Col md={2}></Col>
                <Col md={4}>
                    <p className="margBu"><i> Tsy misy manana ny ampy Fa sambatra izay mifanampy</i></p>
                </Col>
                <Col md={4}>
                    <div className="linkm">
                       <p></p> <label htmlFor=""> <BsFacebook/> <a href="https://www.facebook.com/juditheestelle.randrianitsiriana">  Facebook </a></label>
                       <p><BsLinkedin /> <a href="https://www.linkedin.com/in/lyda-judith-estelle-randrianitsiriana-23a284136/"> Linkedin </a></p>
                       <p><BsMailbox />lydajudithestelle@gmail.com</p>
                       <p><BsFillPhoneVibrateFill />+261347629949</p>
                    </div>
                </Col>
            </Row>
        </div>
    )

}

export default Footer