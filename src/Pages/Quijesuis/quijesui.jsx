import React from "react";
import { Row, Col } from "react-bootstrap";
import Image from 'react-bootstrap/Image';
import './quijesui.css';

const Quijesuis = () =>{

    const quisui = {
        color: "red",
        fontWeith: "100",
        textAlign: "center",
        fontSize: "8vh",
        fontWeight: "100",
       
    }
    const heifont = {
        fontSize: "5vh",
        textAlign:"justify"
    }

    return(
        <div className="marginCl">
            <Row>
                <p style={quisui}> Qui suis-je ?</p>
            </Row>
            <Row className="justify-content-md-center">
                <Col xs lg="2">
                    <Image className="imgquij" src="../Assets/Images/1.jpg" roundedCircle />
                </Col>
                <Col xs lg="3">
                    <p> <i className="heifont">
                        Je suis une femme développeur informatique.

                        J'ai un master en  informatique générale, donc "Ingénierie Logiciel et Bases de Données" et "Systèmes Réseaux".

                        Mais maintenant, j'aime travailler en tant que programmeur et créer des sites Web front-end et back-end.    
                    </i>
                    </p>
                    
                    <p>
                        <i>
                            Je suis strict et créatif.<br/>
                            J'aime travailler en équipe.
                        </i>
                    </p>
                </Col>
            </Row>
        </div>
    )
}
export default Quijesuis;