import React from "react";
import { Row ,Col,Image} from "react-bootstrap";
import './Quecequejefais.css'
import Carousel from 'react-bootstrap/Carousel';
import imc from "../Home/assets/uml.jpg";
import code from "../Home/assets/code.jpg";
import saisir from "../Home/assets/Saisie.jpg";

const Quecequejefais = () => {

    const quisui = {
        color: "red",
        fontWeith: "100",
        textAlign: "center",
        fontSize: "8vh",
        fontWeight: "100",
        marginBlock: "5%"
       
    }

    const containerdi ={
        backgroundColor:"black"
    }
    
    const marginbottonoo = {
        marginBlock: "5%"
    }

    return(
        <div style={containerdi}>
            <Row>
                <p style={quisui}> <i>Qu'est ce que je fais ?</i></p>
            </Row>
            {/* <Row style={marginbottonoo}>
                <Col md={2}></Col>
                <Col md={3}>
                    <p className="csstile">
                        Analyse et conception de données
                    </p>
                    <p>
                        En utilisant le methode UML et le Gestion de projet
                    </p>
                
                </Col>
                <Col md={3}>
                    <p className="csstile">
                        Développeur web
                    </p> 
                    <p>
                        En utilisant les outil et le langage de programmation 
                        <i>PHP, JAVA, C#</i> 
                        <i>HTML, CSS, JS</i>
                        <i>Mysql, MongoDB, SqlServeur</i>
                        <i>Pano2vr, FileZilla, Visual Code </i>
                    </p>
                </Col>
                <Col md={3}>
                    <p className="csstile">
                        Saisir de données 
                    </p>
                    <p>
                        Word, Excel, Power Point.
                    </p>
                
                </Col>
            </Row> */}

            <Row>
                <Col md="2"></Col>
                <Col md="8">
                    <Carousel data-bs-theme="dark">
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={imc}
                            alt="First slide"
                            />
                            <Carousel.Caption>
                            <h5 className="Hanalyse">Analyse et conception de données</h5>
                            <p className="Enutilisa">En utilisant le methode UML et le Gestion de projet</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={code}
                            alt="Second slide"
                            />
                            <Carousel.Caption>
                            <h5 className="Hanalyse">Développeur web</h5>
                            <p className="Enutilisa">
                                En utilisant les outil et le langage de programmation 
                                <i>PHP, JAVA, C#</i> 
                                <i>HTML, CSS, JS</i>
                                <i>Mysql, MongoDB, SqlServeur</i>
                                <i>Pano2vr, FileZilla, Visual Code </i>
                            </p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={saisir}
                            alt="Third slide"
                            />
                            <Carousel.Caption>
                            <h5 className="Hanalyse">Saisir de données </h5>
                            <p className="Enutilisa">
                            Word, Excel, Power Point.
                            </p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
                </Col>
                <Col md="2"></Col>
            </Row>
            <Row>
                <div className="marggg"></div>
            </Row>

        </div>
    )
}
export default Quecequejefais