import React from "react";
import {Row} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './home.css';
import backgroundImgurl from "../Home/assets/lyordi.jpg";

const Home = () =>{

   // const backgroundImgurl = "../assets/Images/lyordi.PNG";
    
    const ContainerStyle = {
        background: `url(${backgroundImgurl})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        //width: '100%',
        height:'80vh',
        filter: `blur(4px)`,
        webkitfilter: `blur(8px)`,
    }

    const Labelp = {
        backgroundcolor: "rgba(0,0,0)",
        backgroundcolor: "rgba(0,0,0, 0.4)",
        color: "white",
        fontweight: "bold",
        //border: "1px solid red",
       // borderRadius: "40% 50%",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        zindex: "2",
        width: "80%",
        padding: "20px",
        textalign: "center",
    }
    const taillRan = {
        fontSize: "26px",
    }

    return (
        <>
        <div style={ContainerStyle}></div>
            <Row style={Labelp}>
                <label htmlFor="">Bonjour</label>
                <label htmlFor="">Je suis  <i style={taillRan}>Lyda RANDRIANITSIRIANA </i></label>
                <p className="devew">Développeur en Informatique</p>
            </Row>
        </>
    )
}
export default Home;