import logo from './logo.svg';
import './App.css';
import Home from "./Pages/Home/home"
import Quijesuis from "./Pages/Quijesuis/quijesui"
import Quecequejefais from "./Pages/Quecequejefais/Quecequejefais"
import Diver from "./Pages/diver/diver"
import Footer from "./Pages/footer/footer"
import Pano from "./Pages/pano/pano"

function App() {
  return (
    <>
       <Home/>
       <Quijesuis/>
       <Quecequejefais/>
       <Pano/>
       <Diver/>
      <Footer/>
    </>
  );
}

export default App;
